import java.util.Scanner;

public class Exercise3 {

    public static void main(String[] args) {
        //Task: Write a Java program that keeps a number from the user and generates an integer between 1 and 7 and displays the name of the weekday.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a number between 1 and 7");
        int day = scanner.nextInt();
        System.out.println(GetDayName(day));
    }
    private static String GetDayName(int day) {

        String dayName;
        switch (day) {
            case 1:
                dayName = "Monday";
                break;
            case 2:
                dayName = "Tuesday";
                break;
            case 3:
                dayName = "Wednesday";
                break;
            case 4:
                dayName = "Thursday";
                break;
            case 5:
                dayName = "Friday";
                break;
            case 6:
                dayName = "Saturday";
                break;
            case 7:
                dayName = "Sunday";
                break;
            default:
                dayName = "Invalid Day";
        }
        return dayName;
    }
}

