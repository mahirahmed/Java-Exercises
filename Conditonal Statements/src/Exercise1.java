import java.util.Scanner;

public class Exercise1 {
    public static void main(String[] args) {

        //Task: Write a Java program to get a number from the user and print whether it is positive or negative.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a number");

        int number = scanner.nextInt();

        if (number % 2 == 0) {

            System.out.println(number + " is positive");
        } else {
            System.out.println(number + " is negative ");
        }
    }
}
