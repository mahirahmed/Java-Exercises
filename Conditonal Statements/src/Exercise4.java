import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Exercise4 {

    public static void main(String[] args) {

        //Task: Write a Java program to find the number of days in a month.

        Scanner scanner = new Scanner(System.in);
        int month, year;
        System.out.println("Please enter the month from 1-12");
        month = scanner.nextInt();
        System.out.println("Please enter the year.");
        year = scanner.nextInt();
        numberOfDaysInMonth(month, year);
    }
    private static void numberOfDaysInMonth(int month, int year) {

        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        String monthName = monthNames[month -1];
        Calendar monthStart = new GregorianCalendar(year, month -1, 1);
        System.out.println(monthName + " " + year + " has " + monthStart.getActualMaximum(Calendar.DAY_OF_MONTH) + " days");

    }
}