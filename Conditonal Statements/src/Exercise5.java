import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Exercise5 {

    public static void main(String[] args) {
        // Task: Write a Java program that takes the user to provide a single character from the alphabet. Print Vowel or Consonant, depending on the user input. If the user input is not a letter (between a and z or A and Z), or is a string of length > 1, print an error message.
        String[] vowel = {"a", "e", "i", "o", "u"};
        List<String> list = Arrays.asList(vowel);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a letter");
        String letter;

        while (scanner.hasNext()) {
                letter = scanner.next();
                if (letter.length() == 1 && !letter.matches("[a-zA-Z ]*\\d+.*")) {
                    if (list.contains(letter)) {
                        System.out.println(letter + " is a vowel");
                    } else {
                        System.out.println(letter + " is a consonant");
                    }
                } else {
                    System.out.println("Please enter a string value that is one character long.");
                    break;
                }
            }
        }
    }