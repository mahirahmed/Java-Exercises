import java.util.Scanner;

public class Exercise6 {

    public static void main(String[] args) {
        //Task: Write a Java program that takes a year from user and print whether that year is a leap year or not.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a year to be checked:");
        int year = scanner.nextInt();
        isItALeapYear(year);
    }

    private static void isItALeapYear(int year) {

        boolean isLeapYear = ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));

        if (isLeapYear)
        {
            System.out.println( year +" is a leap year");
        } else
            System.out.println(year + " is not a leap year.");
    }
}
