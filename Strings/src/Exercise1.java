import java.util.Scanner;

public class Exercise1 {

    public static void main(String[] args) {
        // Task: Write a Java program to get the character at the given index within the String.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a sentence.");
        String str = scanner.nextLine();
        System.out.println("Your sentence is \"" + str + "\"");
        Integer len = str.length();
        System.out.println("Please enter a number between 1-" + len.toString() + " to receive a character at the given index");
        Integer a = scanner.nextInt();
        if (a >= str.length()  && a < 0 || a <str.length() && a < 0) {
            System.out.println("index value does not exist. Please try again.");
        } else {
            returnStringCharacter(str, a - 1);
        }
    }
    private static void returnStringCharacter(String sentence, Integer a) {

        int index1 = sentence.charAt(a);
        System.out.println("The character at position 0 is " +
                (char) index1);
    }
}
