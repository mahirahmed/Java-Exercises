public class Exercise2 {

    public static void main (String[] args){

        // Task: Write a Java program to get the character (Unicode code point) at the given index within the String

        String str = "Java is fun";
        Integer pos = 1;
        getCodePoint(str,  pos );
    }

     private static void getCodePoint(String str,Integer pos ){

        int val = str.codePointAt(pos);
        System.out.println("Character(unicode point) = " + val);
    }
}


