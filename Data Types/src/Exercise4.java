import java.util.Scanner;

public class Exercise4 {

    public static void main(String[] args) {

        // Task: Write a Java program to convert minutes into a number of years and days

        double minutesInYear = 60 * 24 * 365;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of minutes you wish to convert into years and days.");

        double min = scanner.nextDouble();
        long years = (long) (min / minutesInYear);
        int days = (int) (min / 60 / 24) % 365;

        System.out.println((int) min + " minutes is approximately " + years + " years and " + days + " days");

    }
}
