import java.util.Scanner;

public class Exercise1 {
    public static void main(String[] args) {

        //Task: Write a Java program to convert temperature from Fahrenheit to Celsius degree.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a degree in Fahrenheit: ");
        double Fahranheit = scanner.nextDouble();
        FtoC(Fahranheit);
    }

    private static void FtoC(double fahranheit) {

        double celsius = ((fahranheit - 32) / 1.8);
        System.out.println(fahranheit + " degree Fahrenheit is equal to " + Math.round(celsius) + " Celsius");
    }
}
