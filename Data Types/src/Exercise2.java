import java.util.Scanner;

public class Exercise2 {
    public static void main(String[] args) {

        // Task: Write a Java program that reads a number in inches, converts it to meters.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input the inches to convert to meters: ");
        double inches = scanner.nextDouble();
        InchesToMeter(inches);
    }

    private static void InchesToMeter(double inches) {
        double meteres = inches * 0.0254;
        System.out.println(inches + "inch is equal to " + meteres + " meters.");
    }
}
