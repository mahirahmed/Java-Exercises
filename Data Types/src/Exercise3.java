import java.util.Scanner;

public class Exercise3 {

    public static void main(String[] args) {

        // Task: Write a Java program to compute body mass index (BMI).

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input your weight in pounds");
        double weight = scanner.nextDouble();
        System.out.println("Input your height in inches");
        double height = scanner.nextDouble();

        WorkOutBMI(height, weight);
    }

    private static void WorkOutBMI(double inches, double weight) {

        double BMI = weight * 0.45359237 / (inches * 0.0254 * inches * 0.0254);
        System.out.print("Body Mass Index is " + BMI + "\n");
    }
}

