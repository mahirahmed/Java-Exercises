import java.util.Scanner;

public class Exercise5 {

    public static void main(String[] args) {

        //Task: Write a Java program that accepts two integers from the user and then prints the sum, the difference, the         product, the average, the distance (the difference between integer), the maximum (the larger of the two                 integers), the minimum (smaller of the two integers).

        int a, b;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the first integer");
        a = scanner.nextInt();
        System.out.println("And the second integer");
        b = scanner.nextInt();


        workOutEverything(a, b);

    }

    private static void workOutEverything(int a, int b) {

        System.out.printf("Sum of two integers: %d%n", a + b);
        System.out.printf("Difference of two integers: %d%n", a - b);
        System.out.printf("Product of two integers: %d%n", a * b);
        System.out.printf("Average of two integers: %.2f%n", (double) (a + b) / 2);
        System.out.printf("Distance of two integers: %d%n", Math.abs(a - b));
        System.out.printf("Max integer: %d%n", Math.max(a, b));
        System.out.printf("Min integer: %d%n", Math.min(a, b));

    }
}
