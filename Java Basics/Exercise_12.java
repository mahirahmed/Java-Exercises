import java.util.Scanner;

public class Exercise_12 {

    public static void main(String[] args) {
        
        // Task: Write a Java program to count the letters, spaces, numbers and other characters of an input string.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a string to be checked");

        String test = scanner.nextLine();
        count(test);
    }

    private static void count(String stringToCheck) {

        char[] ch = stringToCheck.toCharArray();
        int letter = 0;
        int space = 0;
        int num = 0;
        int other = 0;

        for (int i = 0; i < stringToCheck.length(); i++) {
            if (Character.isLetter(ch[i])) {
                letter++;
            } else if (Character.isSpaceChar(ch[i])) {
                space++;
            } else if (Character.isDigit(ch[i])) {

                num++;
            } else {
                other++;
            }

        }
        System.out.println("The string beings checked: " + stringToCheck);
        System.out.println("letter: " + letter);
        System.out.println("space: " + space);
        System.out.println("number: " + num);
        System.out.println("other: " + other);
    }
}
