import java.util.Scanner;

public class Exercise9 {

    public static void main (String[] args){
        
        // Task :  Write a Java program to compute the area of a hexagon.

        int length;

        Scanner in = new Scanner(System.in);

        System.out.println("Please enter the length of your side.");

        length = in.nextInt();

        System.out.println(3 * Math.sqrt(3)/2 * (Math.pow(length,2)));

    }
}
