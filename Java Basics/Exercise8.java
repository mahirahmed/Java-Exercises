import java.util.Scanner;

public class Exercise8 {

    public static void main(String[] args) {
        
        //Task : Write a Java program to compare two numbers.

        int number1, number2;

        Scanner in = new Scanner(System.in);

        System.out.println("Please enter the first number to be compared:");
        number1 = in.nextInt();

        System.out.println("And the second:");
        number2 = in.nextInt();

        if (number1 == number2) {
            System.out.printf("%d == %d\n", number1, number2);
        }

        if (number1 != number2) {
            System.out.printf("%d != %d\n", number1, number2);
        }

        if (number1 < number2) {
            System.out.printf("%d < %d\n", number1, number2);
        }

        if (number1 > number2) {
            System.out.printf("%d > %d\n", number1, number2);
        }

        if (number1 >= number2) {
            System.out.printf("%d >= %d\n", number1, number2);
        }

        if (number1 <= number2) {
            System.out.printf("%d <= %d\n", number1, number2);
        }


    }


}
