import java.nio.charset.Charset;

public class Exercise_13 {
    public static void main(String[] args) {

        // Task: Write a Java program to list the available character sets in charset objects.
        
        System.out.println("Available Charsets:");

        for (String str : Charset.availableCharsets().keySet()) {
            System.out.println(str);

        }
    }
}
