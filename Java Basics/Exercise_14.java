import java.util.Date;

public class Exercise_14 {
    public static void main(String[] args) {

        // Task:  Write a Java program to display the system time

        Date date = new Date();
        System.out.println("Current Date + Time: " + date.toString());

    }
}
