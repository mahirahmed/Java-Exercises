import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class Exercise_15 {

    public static void main(String[] args) {

        // Task: Write a Java program to display the current date time in specific format.
        // Sample Output:
        // Now: 2017/06/16 08:52:03.066

        SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        date.setCalendar(Calendar.getInstance(TimeZone.getTimeZone("GMT")));
        System.out.println("\nNow: " + date.format(System.currentTimeMillis()));
    }
}
