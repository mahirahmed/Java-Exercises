import java.util.Scanner;
public class Exercise_17 {
    public static void main(String[] args) {

        // Task: Write a Java program to accept a number and check the number is even or not. Prints 1 if even else 0.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a number: ");
        int num = scanner.nextInt();

        if (num % 2 == 0) {
            System.out.println(1);
        } else {
            System.out.println(0);
        }
    }
}