import java.util.Scanner;

public class Exercise5 {

    // Task: Write a Java program to print the area and perimeter of a circle
    
    public static void main(String[] args){

        Scanner in = new Scanner(System.in);
        System.out.print("Enter the radius of your circle.");

        double radius = in.nextDouble();

        System.out.println("The perimeter of the circle is: " + radius*(Math.PI*2)+
                "\nThe area of the circle is: "+ Math.PI * Math.pow(radius,2)
        );

    }
}

